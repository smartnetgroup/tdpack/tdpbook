package books

import "fmt"

func NewBook(title string) int {
	if title != "" {
		fmt.Printf("Создана новая книга 1.0.4 - %s\n", title)
		return 0
	} else {
		fmt.Println("Название книги не может быть пустым!")
		return -1
	}
}
